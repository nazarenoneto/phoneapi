﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.MediaProperties;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace PhoneAPI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class camera : Page
    {
        Windows.Media.Capture.MediaCapture captureMananger;
        public camera()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        async private void btnIniciarPreview_Click(object sender, RoutedEventArgs e)
        {
            captureMananger = new Windows.Media.Capture.MediaCapture();
            await captureMananger.InitializeAsync();
            capturePreview.Source = captureMananger;
            await captureMananger.StartPreviewAsync();
        }

        async private void btnStopPreview_Click(object sender, RoutedEventArgs e)
        {
            await captureMananger.StopPreviewAsync();
        }

        async private void btnCapturar_Click(object sender, RoutedEventArgs e)
        {
            ImageEncodingProperties imgFormat = ImageEncodingProperties.CreateJpeg();
            StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("photo.jpg");

            await captureMananger.CapturePhotoToStorageFileAsync(imgFormat, file);

            BitmapImage bmpImage = new BitmapImage(new Uri(file.Path));
            imagePreview.Source = bmpImage;
        }
    }
}
