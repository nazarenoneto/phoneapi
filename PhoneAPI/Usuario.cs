﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneAPI
{
    class Usuario
    {
        public int codigo { get; set; }
        public int email { get; set; }
        public int login { get; set; }
        public int nome { get; set; }
        public int senha { get; set; }
        public int telefone { get; set; }
    }
}
